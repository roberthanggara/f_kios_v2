<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dummyredirect extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->library("base_url_serv");
		$this->load->library("response_message");
        $this->load->library("encrypt");
	}

	

    public function dummysend(){
    	$dummy_redirect = "";
        if(isset($_GET["layanan"])){
            $layanan = $this->input->get("layanan");
            
            $url = $this->base_url_serv->get_base_url()."get/api/detail_page/json";

			$fields = array(
			   'id_layanan' => $layanan
			);

			$postvars = http_build_query($fields);
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, count($fields));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$result = curl_exec($ch);
			curl_close($ch);

			// print_r($result);
			if($result){
				$data_result = json_decode($result);

				if($data_result->msg_main->status){
					$list_menu = $data_result->msg_detail->list_menu;
					if($list_menu){
						$dummy_redirect = $list_menu->next_page;
					}
				}
			}
        }

        // print_r(base_url().$dummy_redirect);

        redirect(base_url().$dummy_redirect."?menu=".$layanan);

    }


}
?>