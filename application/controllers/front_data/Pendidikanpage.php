<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikanpage extends CI_Controller{

	public function __construct(){
		parent::__construct();

		$this->load->library("base_url_serv");

	}

#=================================================================================================#
#----------------Halaman Utama Pendidikan-----------------#
#=================================================================================================#
	public function pendidikan_jenis($param){
		$data["page"] = "pendidikan_home";

		$url = $this->base_url_serv->get_base_url()."get/api/pendidikan/sekolah/json/".$param;
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close($ch);

		$data["list_menu"] 	= json_decode($result);
		$data["core_url"] 	= $this->base_url_serv->get_base_url();

		// print_r("<pre>");
		// print_r($data);
		$this->load->view("pendidikan/index_pendidikan", $data);
	
	}

#=================================================================================================#
#----------------Halaman Pendidikan Tiap Kecamatan-----------------#
#=================================================================================================#
     public function pendidikan_v_kecamatan(){
	


		$this->load->view("pendidikan/pendidikan_tampil_skl_kecamatan");
	
	}

#=================================================================================================#
#----------------Halaman Detail-----------------#
#=================================================================================================#

	public function pendidikan_detail($pendidikan_jenis, $param){
		$data["page"] = "pendidikan_detail";
		// $param = hash("sha512", "SKL20190509100010");
		// print_r(hash("sha512", "SKL20190509100010"));

		$url = $this->base_url_serv->get_base_url()."get/api/pendidikan/sekolah/json/".$pendidikan_jenis;
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close($ch);

		$data["list_menu"] 	= json_decode($result);


		$url = $this->base_url_serv->get_base_url()."get/api/pendidikan/sekolah/detail/".$param;
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close($ch);

		$data["detail_info"] 	= json_decode($result);
		$data["core_url"] 	= $this->base_url_serv->get_base_url();

		// print_r("<pre>");
		// print_r($data);
		$this->load->view("pendidikan/index_pendidikan", $data);
	
	}
#=================================================================================================#
#-------------------------------------------Homepage_index----------------------------------------#
#=================================================================================================#

}
?>