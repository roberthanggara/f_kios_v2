-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2019 at 09:31 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.0.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kios`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `insert_admin` (`nama` VARCHAR(100), `email` TEXT, `nip` VARCHAR(25), `jabatan` VARCHAR(50), `id_bidang` VARCHAR(12), `lv` INT(2), `pass` VARCHAR(64), `admin_del` VARCHAR(12), `time_update` DATETIME) RETURNS VARCHAR(14) CHARSET latin1 NO SQL
BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from admin 
  	where substr(id_admin,3,6) = left(NOW()+0, 6);
        
  select id_admin into last_key_user from admin
  	where substr(id_admin,3,6) = left(NOW()+0, 6)
  	order by id_admin desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("AD",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat("AD",substr(last_key_user,3,10)+1);
      
  END IF;
  
  
  insert into admin values(fix_key_user, '0', lv, email, pass, '0', nama, nip, jabatan, id_bidang, '0', '0000-00-00 00:00:00', admin_del, time_update);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_dinas` (`nama_dinas` TEXT, `alamat` TEXT, `page_kelola` TEXT, `admin_del` VARCHAR(12), `time_update` DATETIME) RETURNS VARCHAR(8) CHARSET latin1 BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from dinas;
        
  select id_dinas into last_key_item from dinas order by id_dinas desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("DN","200001");
  else
    set fix_key_item = concat("DN",RIGHT(last_key_item,6)+1);
      
  END IF;
  
  insert into dinas values(fix_key_item, nama_dinas, alamat, page_kelola, "0", "0000-00-00 00:00:00", admin_del, time_update);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_jenis_rs` (`nama` VARCHAR(75), `time_update` DATETIME, `id_admin` VARCHAR(11)) RETURNS VARCHAR(70) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from kesehatan_jenis;
        
  select id_layanan into last_key_item from kesehatan_jenis order by id_layanan desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("KSJ","10001");
  else
    set fix_key_item = concat("KSJ",RIGHT(last_key_item,5)+1);
      
  END IF;
  
  insert into kesehatan_jenis values(fix_key_item, nama, "", "0", time_update, id_admin);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_poli` (`nama` VARCHAR(30), `time_update` DATETIME, `id_admin` VARCHAR(11)) RETURNS VARCHAR(70) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from kesehatan_poli;
        
  select id_poli into last_key_item from kesehatan_poli order by id_poli desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("P","10001");
  else
    set fix_key_item = concat("P",RIGHT(last_key_item,5)+1);
      
  END IF;
  
  insert into kesehatan_poli values(fix_key_item, nama, sha2(fix_key_item, '256'), "0", time_update, id_admin);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_rs` (`nama_rs` TEXT, `alamat` TEXT, `foto` VARCHAR(70), `telephone` VARCHAR(13), `id_layanan` VARCHAR(8), `id_poli` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(15) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(15);
  
  select count(*) into count_row_item from kesehatan_rs where substr(id_rs,3,8) = left(NOW()+0, 8);
        
  select id_rs into last_key_item from kesehatan_rs where substr(id_rs,3,8) = left(NOW()+0, 8) order by id_rs desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("RS", left(NOW()+0, 8), "10001");
  else
    set fix_key_item = concat("RS",substr(last_key_item,3,13)+1);
      
  END IF;
  
  insert into kesehatan_rs values(fix_key_item, nama_rs, alamat, foto, telephone, id_layanan, id_poli, "0", waktu, id_admin);
  
  return fix_key_item;
  
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` char(12) NOT NULL,
  `del` enum('0','1') NOT NULL,
  `id_lv` int(2) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `status_active` enum('0','1') NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `id_bidang` varchar(8) NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `del`, `id_lv`, `email`, `password`, `status_active`, `nama`, `nip`, `jabatan`, `id_bidang`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
('AD2019020001', '0', 1, 'kominfo_super@gmail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'Suwardi Surya Ningrat', '19091999012130', 'staff bidang penelitian', 'DN200002', '0', '2019-03-13 08:22:29', '0', '2019-02-21 00:00:00'),
('AD2019030001', '0', 1, 'dinkes@gmail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'Sukiman Sumajono', '123456789011', 'Pengamat lapangan', 'DN200002', '0', '2019-03-13 08:22:20', 'AD2019020001', '2019-03-13 08:17:16');

-- --------------------------------------------------------

--
-- Table structure for table `admin_lv`
--

CREATE TABLE `admin_lv` (
  `id_lv` int(2) NOT NULL,
  `ket` varchar(32) NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_lv`
--

INSERT INTO `admin_lv` (`id_lv`, `ket`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
(1, 'admin super', '0', '0000-00-00 00:00:00', '0', '2019-02-21 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `dinas`
--

CREATE TABLE `dinas` (
  `id_dinas` varchar(8) NOT NULL,
  `nama_dinas` text NOT NULL,
  `alamat` text NOT NULL,
  `page_kelola` text NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dinas`
--

INSERT INTO `dinas` (`id_dinas`, `nama_dinas`, `alamat`, `page_kelola`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
('DN200001', 'Dinas Perdagangan', 'Jl. Simp. Terusan Danau Sentani 3 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200002', 'Dinas Komunikasi dan Informatika (DISKOMINFO)', 'Perkantoran Terpadu Gedung A Lt. 4 Malang, Jl. Mayjend. Sungkono Malang 65132', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200003', 'Dinas Kebudayaan dan Pariwisata (DISBUDPAR)', 'Museum Mpu Purwa, Jl. Sukarno Hatta B. 210 Malang 65142', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200004', 'Dinas Perindustrian (DISPERIN)', 'Perkantoran Terpadu Gedung A Lt.3, Jl. Mayjen Sungkono Malang 65132', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200005', 'Dinas Pendidikan (DISDIK)', 'Jl. Veteran 19 Malang 65145', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200006', 'Dinas Kesehatan (DINKES)', 'Jl. Simp. Laksda Adisucipto 45 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200007', 'Dinas Perhubungan (DISHUB)', 'Jl. Raden Intan 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200008', 'Dinas Pertanian dan Ketahanan Pangan', 'Jl. A. Yani Utara 202 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200009', 'Dinas Perumahan dan Kawasan Pemukiman (DISPERKIM)', 'Jl. Bingkil 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200010', 'Dinas Koperasi dan Usaha Mikro', 'Jl. Panji Suroso 18 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200011', 'Dinas Kepemudaan dan Olahraga (DISPORA)', 'Jl. Tenes Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200012', 'Dinas Kepedudukan dan Pencatatan Sipil (DISPENDUKCAPIL)', 'Perkantoran Terpadu Gedung A Lt. 2, Jl. Mayjen Sungkono Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200013', 'Dinas Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk dan Keluarga Berencana (DP3AP2KB)', 'Jl. Ki Ageng Gribig Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200014', 'Dinas Lingkungan Hidup (DLH)', 'Jl. Mojopahit Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200015', 'Dinas Perpustakaan Umum dan Arsip Daerah', 'Jl. Besar Ijen No.30a Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200016', 'Badan Kesatuan Bangsa dan Politik (BAKESBANGPOL)', 'Jl. Jend. A. Yani 98 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200017', 'Badan Kepegawaian Daerah', 'Jl. Tugu 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200018', 'Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu (DPMPTSP)', 'Perkantoran Terpadu Gedung A Lt. 2 Malang,\r\nJl. Mayjend. Sungkono Malang 65132', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200019', 'Dinas Tenaga Kerja (DISNAKER)', 'Perkantoran Terpadu Gedung B Lt.3, Jl. Mayjen Sungkono Malang 65132', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200020', 'Dinas Pekerjaan Umum dan Penataan Ruang (DPUPR)', ' Jl. Bingkil 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200021', 'Dinas Sosial (DINSOS)', 'Jl. Sulfat No. 12 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200022', 'Inspektorat', 'Jl. Gajah Mada No. 2A Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200023', 'Badan Perencanaan, Penelitian dan Pengembangan (Barenlitbang)', 'Jl. Tugu No. 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200024', 'Badan Pelayanan Pajak Daerah (BP2D)', 'Perkantoran Terpadu Gedung B Lt.1 Jl. Mayjen Sungkono Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200025', 'Badan Pengelola Keuangan dan Aset Daerah (BPKAD)', 'Jl. Tugu 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200026', 'Badan Penanggulangan Bencana Daerah (BPBD)', 'Jl. Jend. A. Yani 98 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200027', 'Satuan Polisi Pamong Praja', 'Jl. Simpang Mojopahit No 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200028', 'Kecamatan Klojen', 'Jl. Surabaya 6 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200029', 'Kecamatan Blimbing', 'Jl. Raden Intan Kav. 14 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200030', 'Kecamatan Lowokwaru', 'Jl. Cengger Ayam I/12 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200031', 'Kecamatan Sukun', 'Jl. Keben I Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200032', 'Kecamatan Kedungkandang', 'Jl. Mayjen Sungkono 59 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200033', 'Badan Pengawas Pemilihan Umum (BAWASLU)', 'Jl. Teluk Cendrawasih No.206, Arjosari, Blimbing, Kota Malang, Jawa Timur 65126', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200034', 'Dewan Perwakilan Rakyat Daerah Kota Malang (DPRD)', 'Jl. Tugu No.1A, Kiduldalem, Klojen, Kota Malang, Jawa Timur 65119', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200035', 'Rumah Potong Hewan Kota Malang', 'Jl. Kol. Sugiono No. 176, Malang, Jawa Timur 65148', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200036', 'PDAM Kota Malang ', 'Jl. Danau Sentani Raya No.100, Madyopuro, Kedungkandang, Kota Malang, Jawa Timur 65142', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200037', 'Komisi Pemilihan Umum Kota Malang (KPU) ', 'Jl. Bantaran No.6, Purwantoro, Blimbing, Kota Malang, Jawa Timur 65126', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200038', 'Bank Perkreditan Rakyat', 'JL. Borobudur no. 18', '', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-02-28 09:48:41'),
('50', 'dinas perbuatan tidak menyenangkan', 'Malang', '', '1', '2019-02-27 03:35:41', 'AD2019020001', '2019-02-27 03:35:24'),
('52', 'surya', 'surya', '', '1', '2019-03-04 02:58:07', 'AD2019020001', '2019-03-01 03:57:29'),
('', 'admin', 'malang', '{}', '0', '0000-00-00 00:00:00', '0', '2019-03-13 10:03:22');

-- --------------------------------------------------------

--
-- Table structure for table `kesehatan_jenis`
--

CREATE TABLE `kesehatan_jenis` (
  `id_layanan` varchar(8) NOT NULL,
  `nama_layanan` text NOT NULL,
  `foto` varchar(70) NOT NULL,
  `is_delete` enum('0','1') DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `id_admin` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kesehatan_jenis`
--

INSERT INTO `kesehatan_jenis` (`id_layanan`, `nama_layanan`, `foto`, `is_delete`, `waktu`, `id_admin`) VALUES
('KSJ10001', 'rumah sakit umum', '41e216aa43a641cac7d611b84323e37dfd67043b28a6d0d50fc16fbd46b189b6.jpg', '0', '2019-03-25 08:53:55', 'AD2019020001'),
('KSJ10002', 'rumah sakit ibu dan anak', '1483bcd0544c8299806b216c188f730ef9e187c076d69c75248f2dce221f8f28.jpg', '0', '2019-03-25 08:51:17', 'AD2019020001'),
('KSJ10003', 'rumah sakit islam', 'b990492b26b993f77bab8fae398eb0909b2ac41e1c942c9e2d7786871ef3c4ce.jpg', '0', '2019-03-25 08:51:42', 'AD2019020001'),
('KSJ10004', 'rumah sakit bersalin', '4a607a6fb0c8c71ca148b9377b0912a4d5f5e6ed9ed6995439e236f7f4cc8d1d.jpg', '0', '2019-03-25 08:52:54', 'AD2019020001'),
('KSJ10005', 'klinik', '113cbc2d33df0f661807223a4533f00de21d8b7e6b2034bc787b26a6ac3dcc68.jpg', '0', '2019-03-25 08:53:06', 'AD2019020001'),
('KSJ10006', 'puskesmas', 'f52d513956d3b4bbbf1b198a74dd7b9fa8e1ef9e52dc0166e86455ecadad3bf1.jpg', '0', '2019-03-25 08:53:20', 'AD2019020001'),
('KSJ10007', 'test 1', 'c25f2a7fafaa546cf1da84ac792f033b9fb1c0deea1ac3d6340055b0405e629c.jpg', '1', '2019-03-25 08:57:04', 'AD2019020001'),
('KSJ10008', 'test 2', '048ef36684ed06d8c259d24a37ac70aefb0eae1dea84482c686cfea246b482c5.jpg', '1', '2019-03-25 08:57:00', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `kesehatan_poli`
--

CREATE TABLE `kesehatan_poli` (
  `id_poli` varchar(11) NOT NULL,
  `nama_poli` varchar(30) NOT NULL,
  `foto` varchar(70) NOT NULL,
  `is_delete` enum('0','1') DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `id_admin` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kesehatan_poli`
--

INSERT INTO `kesehatan_poli` (`id_poli`, `nama_poli`, `foto`, `is_delete`, `waktu`, `id_admin`) VALUES
('P10001', 'mata', '931085c2eaa6b8e4d8f49226d3177ec1522cec30262d0b99bb5d28b7fff0edb5.jpg', '0', '2019-03-25 05:33:43', 'AD2019020001'),
('P10002', 'gigi', '50059ad385a3bd7b793c72d11735b8d0d63ff93e88d284b1c55104006fef17bc.jpg', '0', '2019-03-25 05:33:36', 'AD2019020001'),
('P10003', 'penyakit dalam', '5f36c0d7291d99d01226f8fe60574df9ebb62b97fbe3f6e05848d6ac28f72bab.jpg', '0', '2019-03-25 03:07:55', 'AD2019020001'),
('P10004', 'paru-paru', '6a118570fc84e88aebb381ec8b27ae7d7348efc32220c3c4783e82c8fc89a5f4.jpg', '0', '2019-03-25 05:32:28', 'AD2019020001'),
('P10005', 'jantung', '2ab596154ea6d2f891ccd586291f50849447152e97071737baed6b815136015a.jpg', '0', '2019-03-25 05:32:36', 'AD2019020001'),
('P10006', 'bedah khusus', '2fbc38462a94c056d78ff07e23916c04615c60e90b07153b46c14d527ba92606.jpg', '0', '2019-03-25 05:32:55', 'AD2019020001'),
('P10007', 'bedah ringan', 'bc0a539247035b47948776452eebf9cfbd26b93719677204b6c7f571093181a1.jpg', '0', '2019-03-25 05:33:06', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `kesehatan_rs`
--

CREATE TABLE `kesehatan_rs` (
  `id_rs` varchar(15) NOT NULL,
  `nama_rumah_sakit` text NOT NULL,
  `alamat` text NOT NULL,
  `foto_rs` varchar(70) DEFAULT NULL,
  `telepon` varchar(13) NOT NULL,
  `id_layanan` varchar(8) NOT NULL,
  `id_poli` text NOT NULL,
  `is_delete` enum('0','1') DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `id_admin` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kesehatan_rs`
--

INSERT INTO `kesehatan_rs` (`id_rs`, `nama_rumah_sakit`, `alamat`, `foto_rs`, `telepon`, `id_layanan`, `id_poli`, `is_delete`, `waktu`, `id_admin`) VALUES
('RS2019032710001', 'RSJ', 'malang', '82361e1c39f6b3fd75aa97b9dbe470af7ad1a0d81cb1e5afc28df7091ad67076.jpg', '085841920243', 'KSJ10002', '[\'P10001\',\'P10003\',\'P10006\']', '0', '2019-03-27 04:19:41', 'AD2019020001'),
('RS2019032710002', 'panti nirmala', 'malang', '-', '085841920243', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10005\',\'P10006\']', '0', '2019-03-27 04:39:47', 'AD2019020001'),
('RS2019032710003', 'Rumah Sakti Lavalete', 'malang', '576ef93d7b0f65f037da189f569e938045c26f753d1b42c3a1fbc61bb0ecea64.jpg', '085841920243', 'KSJ10001', '[\'P10001\',\'P10003\',\'P10005\',\'P10007\']', '0', '2019-03-27 04:45:38', 'AD2019020001'),
('RS2019032710004', 'Rumah Sakti Lavalete malang', 'malang', '-', '085841920243', 'KSJ10001', '[\'P10001\',\'P10003\',\'P10005\',\'P10007\']', '0', '2019-03-27 04:46:52', 'AD2019020001'),
('RS2019032710005', 'Rumah Sakti Lavalete malangs', 'malang', '2522405e2d190fa08cb58b6f989941cbad42e0ce4143eba42d9fe392d720fee1.jpg', '085841920243', 'KSJ10001', '[\'P10001\',\'P10003\',\'P10005\',\'P10007\']', '0', '2019-03-27 04:47:29', 'AD2019020001'),
('RS2019032710006', 'Rumah Sakti Lavalete malangsa', 'malang', 'e44c711b36f1fa21b433a5494829a7dab4fd4e1032b19f6348eaf586f76adb00.jpg', '085841920243', 'KSJ10001', '[\'P10001\',\'P10003\',\'P10005\',\'P10007\']', '1', '2019-03-27 09:30:15', 'AD2019020001'),
('RS2019032710007', 'Rumah Sakit Daerah Aisiyah', 'Malang', 'cd933da191a29d2df0f2f26afcac8c3445b81bd38191f81b2689ab367e40b5b1.jpg', '085841920243', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10004\',\'P10005\',\'P10006\']', '1', '2019-03-27 09:30:02', 'AD2019020001');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `admin_lv`
--
ALTER TABLE `admin_lv`
  ADD PRIMARY KEY (`id_lv`);

--
-- Indexes for table `dinas`
--
ALTER TABLE `dinas`
  ADD PRIMARY KEY (`id_dinas`);

--
-- Indexes for table `kesehatan_jenis`
--
ALTER TABLE `kesehatan_jenis`
  ADD PRIMARY KEY (`id_layanan`);

--
-- Indexes for table `kesehatan_poli`
--
ALTER TABLE `kesehatan_poli`
  ADD PRIMARY KEY (`id_poli`);

--
-- Indexes for table `kesehatan_rs`
--
ALTER TABLE `kesehatan_rs`
  ADD PRIMARY KEY (`id_rs`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_lv`
--
ALTER TABLE `admin_lv`
  MODIFY `id_lv` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
